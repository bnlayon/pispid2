<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout', 'LoginUserController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/addproject', 'AddProjectController@addproject')->name('addproject');
Route::post('/addproject', 'AddProjectController@saveproject');
Route::get('/viewproject', 'ViewProjectController@viewproject')->name('viewproject');
Route::get('/editproject/{id}', 'EditProjectController@editproject')->name('editproject');
Route::post('/editproject/{id}', 'EditProjectController@editsave');
Route::get('/report', 'ReportController@report')->name('report');

Route::get('export', 'ReportController@export')->name('export');
Route::post('export_filter', 'ReportController@export_filter')->name('export_filter');


Route::get('/admin', 'AdminController@admin')->name('admin');
Route::post('/deactivate/{id}', 'AdminController@deactivate');
Route::post('/activate/{id}', 'AdminController@activate');

Route::get('/profile', 'ProfileController@profile')->name('profile');

Route::get('importExportView', 'ReportController@importExportView');
Route::post('import', 'ReportController@import')->name('import');

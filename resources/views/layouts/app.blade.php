<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <link rel="icon" href="{{ asset('img/neda-logo.png') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<!--     <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script>
    $(document)
      .ready(function() {
        // fix main menu to page on passing
        $('.main.menu').visibility({
          type: 'fixed'
        });
        $('.overlay').visibility({
          type: 'fixed',
          offset: 80
        });
        // lazy load images
        $('.image').visibility({
          type: 'image',
          transition: 'vertical flip in',
          duration: 500
        });

        // show dropdown on hover
        $('.main.menu  .ui.dropdown').dropdown({
          on: 'hover'
        });
      })
    ;
    </script>
    <style>

     .error {
      color: red;
      background-color: #acf;
      }
 /*       .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url({{ asset('img/Preloader_1.gif') }}) center no-repeat #fff;
        }*/

          .main.container {
            margin-top: 2em;
          }

          .main.menu {
            border-radius: 0;
            border: none;
            box-shadow: none;
            transition:
              box-shadow 0.5s ease,
              padding 0.5s ease
            ;
          }
          .main.menu .item img.logo {
            margin-right: 1.5em;
          }

          .overlay {
            float: left;
            margin: 0em 3em 1em 0em;
          }
          .overlay .menu {
            position: relative;
            left: 0;
            transition: left 0.5s ease;
          }

          .main.menu.fixed {
            background-color: #FFFFFF;
            border: 1px solid #DDD;
            box-shadow: 0px 3px 5px rgba(0, 0, 0, 0.2);
          }
          .overlay.fixed .menu {
            left: 800px;
          }

          .text.container .left.floated.image {
            margin: 2em 2em 2em -4em;
          }
          .text.container .right.floated.image {
            margin: 2em -4em 2em 2em;
          }

          .ui.footer.segment {
            margin: 5em 0em 0em;
            padding: 5em 0em;
          }

          .modal-body{
            height: 200px;
          overflow-y: auto;
          }
          .w3-container,.w3-panel{padding:0.01em 16px}.w3-panel{margin-top:16px;margin-bottom:16px}
          .w3-modal{z-index:3;display:none;padding-top:100px;position:fixed;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:rgb(0,0,0);background-color:rgba(0,0,0,0.4)}
          .w3-modal-content{margin:auto;background-color:#fff;position:relative;padding:0;outline:0;width:1000px}
          @media (max-width:600px){.w3-modal-content{margin:0 10px;width:auto!important}.w3-modal{padding-top:30px}
          @media (max-width:768px){.w3-modal-content{width:500px}.w3-modal{padding-top:50px}}
          @media (min-width:993px){.w3-modal-content{width:900px}.w3-hide-large{display:none!important}.w3-sidebar.w3-collapse{display:block!important}}




    </style>     


<!--     <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
            $(window).load(function() {
                $(".se-pre-con").fadeOut("slow");;
            });
    </script> -->
<!--     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- DATA TABLES -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <!-- SEMANTICS  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/site.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/container.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/grid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/header.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/input.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/button.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/divider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/label.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/dropdown.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/transition.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/step.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/sticky.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/table.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/components/form.css') }}">    
    <script src="{{ asset('assets/library/iframe-content.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/popup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/dropdown.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/transition.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/modal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/visibility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/components/modal.min.js') }}"></script>


  <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
  <script type="text/javascript" src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

  <script type="text/javascript">
      $(document)
        .ready(function() {
          $('.ui.dropdown')
            .dropdown({
              on: 'click'
            })
          ;
        })
      ;
  </script>

    <script type="text/javascript">
    $(document).ready(function() {
      $('#projects').DataTable();
  } );

    $(document).ready(function() {
      $('#users').DataTable();
  } );
  </script>


    <!-- End of Loader -->
</head>

<body>
    <!-- Loader DIV -->
    <div class="se-pre-con"></div>
    <!-- Ends -->
    <div id="app">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link"  href="{{ url('/home') }}">
                <img src="{{ asset('img/neda-logo.png') }}" class="img-responsive" height="20">&nbsp;&nbsp;{{ config('app.name') }}

              </a>
            </li>
            @guest
            @else
            <li class="nav-item">
              <a class="nav-link" href="/addproject">Add Project</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/viewproject">View Projects</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/report">Report Generation</a>
            </li>
            <?php if(Auth::user()->role == 'AD'){ ?>
            <li class="nav-item">
              <a class="nav-link" href="/admin">Administration</a>
            </li>
          <?php } ?>
          </ul>
          <ul class="navbar-nav ml-auto">
            <div class="ui dropdown">
              <div class="nav-link"><i class="bell icon"></i>
              </div>
              <div class="menu">
                <div class="item">
                  <a class="dropdown-item" href="{{ route('logout') }}">
                    Notification 1 - Lorem ipsum dolor sit amet.
                  </a>
                </div>
                <div class="item">
                  <a class="dropdown-item" href="{{ route('logout') }}">
                    Notification 1 - Lorem ipsum dolor sit amet.
                  </a>
                </div>
                <div class="item">
                  <a class="dropdown-item" href="{{ route('logout') }}">
                    Notification 1 - Lorem ipsum dolor sit amet.
                  </a>
                </div>
              </div>
            </div>
            <div class="ui dropdown">
              
              <div class="nav-link"><img src="{{ asset('assets/images/avatar/tom.jpg') }}" class="img-responsive" height="20">&nbsp;&nbsp;{{ Auth::user()->name }}<i class="dropdown icon"></i>
              </div>
              <div class="menu">
                <div class="item">
                  <a class="dropdown-item" href="{{ route('profile') }}">
                    My Profile
                  </a>
                </div>
                <div class="item">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>

              </div>
            </div>
          </ul>
          @endguest
        </div>
      </nav>  

        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div id="loader"></div>
                    @yield('content')
                    @include('sweet::alert')
                </div>
            </div>
        </main>
    </div>
</body>

</html>

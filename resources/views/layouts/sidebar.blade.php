<div class="col-2">
<div class="card mb-3">

  <img style="height: 160px; width: 100%; display: block;" src="{{ asset('assets/images/avatar/tom.jpg') }}">
  <div class="list-group" align="center">

    <a href="/home" class="list-group-item list-group-item-action
    @if (url()->current() == 'http://localhost:8000/home')
    	active
    @endif
    ">My Dashboard</a>

    <a href="/addproject" class="list-group-item list-group-item-action 
    @if (url()->current() == 'http://localhost:8000/addproject')
    	active
    @endif
    ">Add Project</a>

    <a href="/viewproject" class="list-group-item list-group-item-action 
    @if (url()->current() == 'http://localhost:8000/viewproject')
        active
    @endif

    @if(strpos(url()->current(), 'editproject') !== false){
        active
    @endif
    ">View Project</a>

    <a href="#" class="list-group-item list-group-item-action">Report Generation</a>

    <a href="#" class="list-group-item list-group-item-action">Administration</a>

  </div>
</div>

</div>


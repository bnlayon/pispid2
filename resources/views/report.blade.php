@extends('layouts.app')

@section('content')

@if(Auth::user()->active == 0)
  <meta http-equiv="refresh" content="0;url=http://localhost:8000/logout" />
@endif
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Report Generation</li>
                </ol>
                </div>
            </div>

            <div class="ui two column grid">
              <div class="column">
                <div class="ui raised segment">


                  <form action="{{ asset('export_filter') }}" method="POST" id="main_form" class="form ui" novalidate="">{{ csrf_field() }}
                  <a class="ui blue ribbon label">Generate Projects with Filters</a>
                  <div class="form-group field">
                      <br>
                      <label for="sfinancing">Choose Columns to Display</label>
                      <select class="ui fluid search dropdown" name="field[]" id="field" multiple="">
                      <?php foreach ($fields as $key => $value){
                        if($value == 'id'){
                          echo "<option value='projects.id'>".'projects.id'."</option>";
                        }
                           echo "<option value='$value'>".$value."</option>";
                      } ?>
                      </select> 
                    </div>
                    <div class="form-group field">
                      <br>
                      <label for="sfinancing">Source of Financing</label>
                      <select class="ui fluid search dropdown" name="source">
                      <option value="">Choose</option>
                          @foreach ($sources as $source)
                            <option value="{{ $source->id }}" id="{{ $source->id }}">{{ $source->type }}</option>
                          @endforeach
                      </select> 
                    </div>
                    <div class="custom-control  custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="IFP_switch" name="IFP_switch" value="1">
                        <label class="custom-control-label" for="IFP_switch"><b>IFP</b></label>
                    </div>
                    <div class="custom-control  custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="PIP_switch" name="PIP_switch" value="1">
                        <label class="custom-control-label" for="PIP_switch"><b>PIP</b></label>
                    </div>
                    <div class="form-group field">
                      <label for="iagency" class="adjust">Implementing Agency</label>
                        <select class="form-control ui dropdown" id="iagency" name="iagency">
                          <option disabled="" selected="" value="">Choose</option>
                          @foreach ($agencies as $agency)
                            <option id="{{ $agency->id }}" value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group field" id="currentlevel_icc">
                      <label class="control-label">Current Level of ICC Review</label>
                      <select class="form-control ui dropdown" id="currentlevel" name="currentlevel">
                        <option value="">Choose</option>
                        @foreach ($levels as $level_)
                          <option id="{{ $level_->id }}" value="{{ $level_->id }}">{{ $level_->level }}</option>
                        @endforeach
                      </select>
                    </div>
                    <span>
                    <button class="huge ui green icon button">
                    <i class="file excel outline icon"></i></button>
                  </span>
                  </form>
                </div>
              </div>
            </div>

        

@endsection


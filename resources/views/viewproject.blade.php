@extends('layouts.app')

@section('content')
@if(Auth::user()->active == 0)
  <meta http-equiv="refresh" content="0;url=http://localhost:8000/logout" />
@endif

        <div class="col-12">
            <div class="row">
                <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">View Project</li>
                </ol>
                </div>
            </div>

            <div class="row">
            	<div class="col-12">
        				<table id="projects" class="display ui striped table" style="width:100%">
        				        <thead>
        				            <tr>
        				                <th>Project ID</th>
        				                <th>Title</th>
        				                <th>Implementing Agency</th>
        				                <th>Status</th>
                                <th>ICC Level of Approval</th>
        				                <th>Actions</th>
        				            </tr>
        				        </thead>
        				        <tbody>
        				        	@foreach ($projects as $project)
        				        	<tr>
        				        		<td>{{ $project->id }}</td>
        				        		<td>{{ $project->project_title }}</td>
        				        		<td>{{ $project->UACS_AGY_DSC }}</td>
        				        		<td>{{ $project->status }}</td>
                            <td>{{ $project->level }}</td>
        				        		<td><button onclick="document.getElementById('id{{ $project->id }}').style.display='block'" class="mini positive ui button">View</button>

                              <?php 

                              $id = encrypt($project->id);

                              ?>
                              <button onclick=" window.open('{{ asset('/editproject') }}/{{ $id }}','_blank')" class="mini ui button">Edit</button>


                            <div id="printid{{ $project->id }}" style="display: none;">
                              <div class="column">
                                      <div class="ui raised segment">
                                        <a class="ui red ribbon label"><b>Project Title</b></a>
                                        <p>{{ $project->project_title }}</p>
                                        <a class="ui blue ribbon label"><b>Implementing Agency</b></a>
                                        <p>{{ $project->UACS_AGY_DSC }}</p>
                                        <a class="ui green ribbon label"><b>Project Cost</b></a>
                                        <p>{{ $project->cost_tpc }}</p>
                                        <a class="ui orange ribbon label"><b>Financing Source</b></a>
                                        <p>{{ $project->type }}</p>
                                        <a class="ui green ribbon label"><b>Project Status</b></a>
                                        <p>{{ $project->status }}</p>
                                        <a class="ui blue ribbon label"><b>Executive Talk Points</b></a>
                                        <p>{{ $project->talkpoints }}</p>
                                        <a class="ui pink ribbon label"><b>Chronology of Events</b></a>
                                        <div class="modal-body">
                                        <table class="ui fixed table ">
                                          <thead>
                                            <tr>
                                            <th>Date</th>
                                            <th>Remarks</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                        @foreach ($project->getChronoDates($project->id) as $chronos)
                                        <tr>
                                          <td width="15%">{{ $chronos->date }}</td>
                                          <td>{{ $chronos->remarks }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                        </table>
                                        </div>
                                      </div>
                                      <br>
                                    </div>
                            </div>  
                            <div id="id{{ $project->id }}" class="w3-modal w3-animate-opacity" style="top: -50px;">
                              <div class="w3-modal-content">
                                <div class="w3-container ">
                                  <br>
                                  <button class="positive ui button" onclick="printDiv{{ $project->id }}()"><i class="print icon"></i>Print</button>
                                  <span onclick="document.getElementById('id{{ $project->id }}').style.display='none'" class="w3-button w3-display-topright"><i class="window close icon"></i></span>
                                  <br><br>
                                    <div class="column">
                                      <div class="ui raised segment">
                                        <a class="ui red ribbon label">Project Title</a>
                                        <p>{{ $project->project_title }}</p>
                                        <a class="ui blue ribbon label">Implementing Agency</a>
                                        <p>{{ $project->UACS_AGY_DSC }}</p>
                                        <a class="ui green ribbon label">Project Cost</a>
                                        <p>{{ $project->cost_tpc }}</p>
                                        <a class="ui orange ribbon label">Financing Source</a>
                                        <p>{{ $project->type }}</p>
                                        <a class="ui green ribbon label">Project Status</a>
                                        <p>{{ $project->status }}</p>
                                        <a class="ui blue ribbon label">Executive Talk Points</a>
                                        <p>{{ $project->talkpoints }}</p>
                                        <a class="ui pink ribbon label">Chronology of Events</a>
                                        <div class="modal-body">
                                        <table class="ui fixed table ">
                                          <thead>
                                            <tr>
                                            <th>Date</th>
                                            <th>Remarks</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                        @foreach ($project->getChronoDates($project->id) as $chronos)
                                        <tr>
                                          <td width="15%">{{ $chronos->date }}</td>
                                          <td>{{ $chronos->remarks }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                        </table>
                                        </div>
                                      </div>
                                      <br>
                                      <script> 
                                            function printDiv{{ $project->id }}() { 
                                                var divContents = document.getElementById("printid{{ $project->id }}").innerHTML; 
                                                var a = window.open('', '', 'height=500, width=500'); 
                                                a.document.write(divContents); 
                                                a.document.close(); 
                                                a.print(); 
                                            } 
                                        </script> 
                                        
                                    </div>
                                    <br>
                                </div>
                              </div>
                            </div>
                          </td>
        				        	</tr>

        				        	@endforeach
        				        </tbody>
        				        <tfoot>
        				            <tr>
        				                <th>Project ID</th>
        				                <th>Title</th>
        				                <th>Implementing Agency</th>
        				                <th>Status</th>
        				                <th>ICC Level of Approval</th>
                                <th>Actions</th>
        				            </tr>
        				        </tfoot>
        				</table>
      				</div>
                  </div>
              </div>


<style>
   .w3-animate-opacity {
   animation: opac 0.8s
 }
 
 @keyframes opac {
   from {
     opacity: 0
   }
   to {
     opacity: 1
   }
 }
 
 .w3-animate-show {
   animation: show 0.8s;
   animation-fill-mode: forwards;
 }
 
 @keyframes show {
   0% {
     opacity: 1
   }
   100% {
     opacity: 0
   }
 }
</style>

@endsection


@extends('layouts.app')

@section('content')

@if(Auth::user()->active == 0)
  <meta http-equiv="refresh" content="0;url=http://localhost:8000/logout" />
@endif
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>

        <div class="col-12">
            <div class="row">
                <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Add Project</li>
                </ol>
                </div>
            </div>
            
        <form action="{{ asset('/addproject') }}" method="POST" id="main_form" class="form ui" novalidate="">{{ csrf_field() }}
          <center>
          <div class="ui tiny steps">
            <a class="step" href="#pi">
              <i class="file archive icon"></i>
              <div class="content">
                <div class="title">Project Information</div>
                <div class="description"></div>
              </div>
            </a>
            <a class="step" href="#fh">
              <i class="money bill alternate icon"></i>
              <div class="content">
                <div class="title">Financial History</div>
                <div class="description"></div>
              </div>
            </a>
            <a class="step" href="#it">
              <i class="random icon"></i>
              <div class="content">
                <div class="title">ICC Tracking</div>
                <div class="description"></div>
              </div>
            </a>
            <a class="step" href="#fd">
              <i class="ruble sign icon"></i>
              <div class="content">
                <div class="title">Financing Details</div>
                <div class="description"></div>
              </div>
            </a>
            <a class="step" href="#coe">
              <i class="sort numeric down icon"></i>
              <div class="content">
                <div class="title">Chronology</div>
                <div class="description"></div>
              </div>
            </a>
            <a class="step" href="#coe">
              <div class="content">
                <div class="title"><button class="ui primary submit button">Submit</button></div>
                <div class="description"></div>
              </div>
            </a>
          </div>
          </center>
          <br>
            <div class="row">
              <div class="col-12">

               
                    <div id="myTabContent" class="tab-content">
                    <!-- Project Information -->

                      <div id="pi">
                      <a class="ui blue image label">
                        I. 
                        <div class="detail">PROJECT INFORMATION</div>
                      </a>
                        <div class="container">
                            <br>
                            <div class="form-group field">
                              <label for="title"><i class="flag icon blue"></i>Project Title</label>
                              <input type="text" class="form-control" id="project_title" aria-describedby="title" name="project_title">
                            </div>
                            <div class="form-group field">
                              <label for="objectives"><i class="flag icon blue"></i>Project Objectives</label>
                              <textarea class="form-control" id="objectives" rows="3" name="objectives"></textarea>
                            </div>
                            <div class="form-group field">
                              <label for="description"><i class="flag icon blue"></i>Project Description</label>
                              <textarea class="form-control" id="description" rows="3" name="description"></textarea>
                            </div>
                            <div class="form-group field">
                              <label for="talkpoints"><i class="flag icon blue"></i>Executive Talk Points</label>
                              <textarea class="form-control" id="talkpoints" rows="3" name="talkpoints"></textarea>
                            </div>
                            <div class="form-group field">
                              <label for="status"><i class="flag icon blue"></i>Project Status</label>
                              <textarea class="form-control" id="status" rows="3" name="status"></textarea>
                            </div>
                            <div class="form-group field">
                              <label for="ProponentAgency"><b>Proponent Agency</b></label>
                              <label for="iagency"><i class="flag icon blue"></i>Implementing Agency</label>
                                  <select class="form-control ui" id="iagency" name="iagency">
                                      <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($agencies as $agency)
                                      <option id="{{ $agency->id }}" value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                                <label for="coiagency" class="adjust">Co-Implementing Agency</label>
                                  <select class="form-control ui dropdown" id="coiagency" multiple="" name="coiagency[]">
                                    @foreach ($agencies as $agency)
                                      <option id="{{ $agency->id }}" value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                                <label for="aagency" class="adjust">Attached Agency</label>
                                  <select class="form-control ui dropdown" id="aagency" name="aagency">
                                    <option disabled="" selected="">Choose</option>
                                    @foreach ($agencies as $agency)
                                      <option id="{{ $agency->id }}" value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                            <div class="row">
                                  <div class="form-group field col-4">                        
                                    <label class="control-label"><b>Agency Focal</b></label>
                                    <input type="text" class="form-control" id="agency_focal" name="agency_focal">
                                  </div>
                                  <div class="form-group field col-4">                        
                                    <label class="control-label"><b>Email Address</b></label>
                                    <input type="text" class="form-control" id="agency_email" name="agency_email">
                                  </div>
                                  <div class="form-group field col-4">                        
                                    <label class="control-label"><b>Contact Number</b></label>
                                    <input type="text" class="form-control" id="agency_number" name="agency_number">
                                  </div>
                            </div>
                            </div>
<!--                             <div class="form-group field">
                              <label for="sfinancing"><i class="flag icon blue"></i>Source of Financing</label>
                                  <select class="form-control ui dropdown" id="sfinancing" onchange="java_script_:showSourceOfFinancing(this.options[this.selectedIndex].value)" name="sfinancing">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($sources as $source)
                                      <option value="{{ $source->id }}" id="{{ $source->id }}">{{ $source->type }}</option>
                                    @endforeach
                                  </select>
                            </div> -->
                            <div class="form-group field">
                                <label for="sfinancing"><i class="flag icon blue"></i>Source of Financing</label>
                                <select class="form-control" id="sfinancing" onchange="java_script_:showSourceOfFinancing(this.options[this.selectedIndex].value)" name="sfinancing">
                                <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($sources as $source)
                                      <option value="{{ $source->id }}" id="{{ $source->id }}">{{ $source->type }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group field" id="others1" style="display: none;">
                              <label for="finance_others"><i class="flag icon blue"></i>Other Source of Financing (please specify)</label>
                              <input type="text" class="form-control" id="finance_others" name="finance_others">
                            </div>
                            <div class="form-group field" id="oda2" style="display: none;">
                              <label for="oda"><i class="flag icon blue"></i>ODA Type</label>
                                  <select class="form-control" id="oda_type" name="oda_type">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($loantypes as $loantype)
                                      <option value="{{ $loantype->id }}" name="{{ $loantype->id }}">{{ $loantype->type }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field" id="ppp2" style="display: none;">
                              <label for="ppp"><i class="flag icon blue"></i>PPP Type</label>
                                  <select class="form-control" id="ppp_type" name="ppp_type" onchange="java_script_:showPPPVariance(this.options[this.selectedIndex].value)">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($ppptypes as $ppptype)
                                      <option value="{{ $ppptype->id }}" id="{{ $ppptype->id }}">{{ $ppptype->type }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field" id="ppp_type_others_field" style="display: none;">
                              <label for="finance_others"><i class="flag icon blue"></i>Other PPP Type (please specify)</label>
                              <input type="text" class="form-control" id="ppp_type_others" name="ppp_type_others">
                            </div>
                            <div class="form-group field" id="ppp_var" style="display: none;">
                              <label for="ppp"><i class="flag icon blue"></i>PPP Variance</label>
                                  <select class="form-control" id="ppp_variance" name="ppp_variance" onchange="java_script_:showOtherVariance(this.options[this.selectedIndex].value)">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($variances as $variance_)
                                      <option value="{{ $variance_->id }}" id="{{ $variance_->id }}">{{ $variance_->variance }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field" id="others3" style="display: none;">
                              <label for="finance_others"><i class="flag icon blue"></i>Other PPP Variance (please specify)</label>
                              <input type="text" class="form-control" id="ppp_others" name="ppp_others">
                            </div>
                            <div class="form-group field">
                              <label for="sector"><i class="flag icon blue"></i>Main Sector</label>
                                  <select class="form-control" id="sector" name="sector" onchange="java_script_:showSubsector(this.options[this.selectedIndex].value)">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($sectors as $sector)
                                      <option id="{{ $sector->id }}" value="{{ $sector->id }}">{{ $sector->sector }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                                <label for="osectors" class="adjust">Other Sectors</label>
                                  <select class="form-control ui dropdown" id="osectors" multiple="" name="osectors[]">
                                    @foreach ($sectors as $sector)
                                      <option id="{{ $sector->id }}" value="{{ $sector->id }}">{{ $sector->sector }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group">
                              <label for="subsector"><i class="flag icon blue"></i>Subsector</label>
                              <div class="row">
                                <?php for($i=1;$i<=5;$i++){ ?>
                                  <div id="sector_{{ $i }}" style="display: none;" class="input-group col-6"> 
                                @foreach ($subsectors as $subsectors_)
                                @if ($subsectors_->sector == $i)
                                <div class="custom-control custom-checkbox col-12">
                                  <input type="checkbox" class="custom-control-input" name="subsector[]" value="{{ $subsectors_->id }}" id="{{ $subsectors_->subsector }}">
                                  <label class="custom-control-label" for="{{ $subsectors_->subsector }}"><b>{{ $subsectors_->subsector }}</b></label>
                                </div>
                                @endif
                                @endforeach
                              </div>
                                <?php } ?>
                              </div>
                            </div>
                            <div class="form-group field">
                              <label for="estafflead">Evaluating Staff (Lead)</label>
                                  <select class="form-control ui dropdown" id="estafflead" name="estafflead">
                                    <option disabled="" selected="">Choose</option>
                                    @foreach ($leadevaluatingstaffs as $leadevaluatingstaff)
                                      <option id="{{ $leadevaluatingstaff->id }}" value="{{ $leadevaluatingstaff->id }}">{{ $leadevaluatingstaff->abbrv }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                              <label for="estaff">Evaluating Staffs</label>
                                  <select class="form-control ui dropdown" id="estaff" multiple="multiple" name="estaff[]">
                                    <option disabled="" selected="">Choose</option>
                                    @foreach ($evaluatingstaffs as $evaluatingstaff)
                                      <option id="{{ $evaluatingstaff->id }}" value="{{ $evaluatingstaff->id }}">{{ $evaluatingstaff->abbrv }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field">
                              <label for="es_division">Evaluating Divisions</label>
                              <input type="text" class="form-control" id="es_division" aria-describedby="es_division" name="es_division">
                            </div>
                            <label for="ProponentAgency"><b>Implementation Period (as submitted)</b></label>
                            <div class="row">
                                
                                  <div class="form-group field col-6">  
                                      <label for="impdate"><i class="flag icon blue"></i>Implementation Date (To)</label>
                                      <input type="date" class="form-control" id="impdate" name="impdate">
                                  </div>
                                  <div class="form-group field col-6">  
                                      <label for="impdate2"><i class="flag icon blue"></i>Implementation Date (From)</label>
                                      <input type="date" class="form-control" id="impdate2" name="impdate2">
                                  </div>
                            </div>

                            <div class="form-group field">
                              <label for="spatial"><i class="flag icon blue"></i>Spatial Coverage</label>
                                  <select class="form-control" id="spatial" onchange="java_script_:showSpatial(this.options[this.selectedIndex].value)" name="spatial">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($spatials as $spatial_)
                                      <option value="{{ $spatial_->id }}">{{ $spatial_->spatial }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field" id="if_inter" style="display: none;">
                              <label for="interregional">Choose Regions</label>
                                  <select class="form-control ui dropdown" id="interregional" multiple="" name="interregional[]">
                                    <option disabled="" selected="">Choose</option>
                                    @foreach ($regions as $region)
                                      <option id="{{ $region->region_no }}" value="{{ $region->region_no }}">{{ $region->region_description }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group field" id="if_reg" style="display: none;">
                              <label for="regionspecific">Choose Region</label>
                                  <select class="form-control ui dropdown" id="region" name="region">
                                    <option disabled="" selected="">Choose</option>
                                    @foreach ($regions as $region)
                                      <option id="{{ $region->region_no }}" value="{{ $region->region_no }}">{{ $region->region_description }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                      </div>
                    <!-- End of Project Information -->
                    <!-- Financial History -->
                      <div id="fh">
                      <a class="ui teal image label">
                        II. 
                        <div class="detail">FINANCIAL HISTORY</div>
                      </a>
                        <div class="container">
<!--                           <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Costs (as submitted)</b></div>
                                <div class="card-body">
                                  <div class="form-group field">  
                                        <label class="control-label">Foreign Counterpart (in million PhP)</label>
                                        <input type="number" class="form-control" id="cost_fc" name="cost_fc">
                                  </div>
                                  <div class="form-group field">                     
                                        <label class="control-label">Local Counterpart (in million PhP)</label>
                                        <input type="number" class="form-control" id="cost_lc" name="cost_lc">
                                  </div>
                                  <div class="form-group field">
                                        <label class="control-label">Private Counterpart (in million PhP)</label>
                                        <input type="number" class="form-control" id="cost_pc" name="cost_pc">
                                  </div>
                                  <div class="form-group field">                        
                                    <label class="control-label"><i class="flag icon blue"></i>Total Project Cost (in million PhP)</label>
                                    <input type="number" class="form-control" id="cost_tpc" name="cost_tpc">
                                  </div>
                                  </div>
                              </div>
                            </div>
                          </div> -->
<!--                           <br> -->
<!--                      <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Original Cost</b></div>
                                <div class="card-body">
                                  <div class="form-group field">                        
                                        <label class="control-label">LP/GP/PS Financing (in million PhP)</label>
                                        <input type="number" class="form-control" id="oc_f" name="oc_f">
                                  </div>
                                  <div class="form-group field">                        
                                        <label class="control-label">Total Project Cost (in million PhP)</label>
                                        <input type="number" class="form-control" id="oc_tpc" name="oc_tpc">
                                  </div>
                                  <div class="form-group field">
                                      <label class="control-label">Original currency</label>
                                      <select class="form-control" id="oc_currency" name="oc_currency">
                                        <option disabled="" selected="" value="">Choose</option>
                                        @foreach ($currency as $currenc)
                                          <option id="{{ $currenc->id }}" value="{{ $currenc->id }}">{{ $currenc->currency }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group field">
                                      <label class="control-label">Amount in Original Currency (in million PhP)</label>
                                      <input type="number" class="form-control" id="oc_amount_orig_currency" name="oc_amount_orig_currency">
                                  </div>
                                  <div class="form-group field">
                                      <div class="input-group col-12">
                                        <label class="control-label col-3 adjust">Currency to US$1 =</label>
                                        <input type="number" class="form-control col-2" id="oc_c_dol" name="oc_c_dol">
                                        <label class="control-label col-3 adjust">US$1 to PhP =</label>
                                        <input type="number" class="form-control col-2" id="oc_d_php" name="oc_d_php">
                                      </div>
                                  </div>
                                  <div class="form-group field">
                                    <label for="title">Remarks</label>
                                    <input type="text" class="form-control" id="oc_remarks" aria-describedby="oc_remarks" name="oc_remarks">
                                  </div>
                                  </div>
                              </div>
                            </div>
                          </div> -->
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Cost</b></div>
                                <div class="card-body">
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">LP/GP/PS Financing (in million PhP)</label>
                                        <input type="number" class="form-control" id="ec_f" name="ec_f">
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label">GPH Counterpart (in million PhP)</label>
                                        <input type="number" class="form-control" id="ec_gdp" name="ec_gdp">
                                  </div>
                                  </div>
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">Other Costs (in million PhP)</label>
                                        <input type="number" class="form-control" id="ec_oc" name="ec_oc">
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label"><i class="flag icon blue"></i>Total Cost (in million PhP)</label>
                                        <input type="number" class="form-control" id="ec_tec" name="ec_tec">
                                  </div>
                                  </div>
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">Original currency (for Loans)</label>
                                        <select class="form-control ui dropdown" id="ec_currency" name="ec_currency">
                                        <option disabled="" selected="">Choose</option>
                                        @foreach ($currency as $currenc)
                                          <option id="{{ $currenc->id }}" value="{{ $currenc->id }}">{{ $currenc->currency }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label">Amount in Original Currency</label>
                                        <input type="number" class="form-control" id="ec_amount_orig_currency" name="ec_amount_orig_currency">
                                  </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="input-group col-12">
                                        <label class="control-label col-3 adjust">Currency to US$1 =</label>
                                        <input type="number" class="form-control col-2" id="ec_c_dol" name="ec_c_dol">
                                        <label class="control-label col-3 adjust">US$1 to PhP =</label>
                                        <input type="number" class="form-control col-2" id="ec_d_php" name="ec_d_php">
                                      </div>
                                  </div>
                                  <div class="form-group field">
                                    <label for="title">Remarks</label>
                                    <input type="text" class="form-control" id="ec_remarks" aria-describedby="ec_remarks" name="ec_remarks">
                                  </div>
                                  </div>
                              </div>
                            </div>
                          </div>
<!--                           <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Committed Cost</b></div>
                                <div class="card-body">
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">LP/GP/PS Financing (in million PhP)</label>
                                        <input type="number" class="form-control" id="cc_f" name="cc_f">
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label">GPH Counterpart (in million PhP)</label>
                                        <input type="number" class="form-control" id="cc_gdp" name="cc_gdp">
                                  </div>
                                  </div>
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">Other Costs (in million PhP)</label>
                                        <input type="number" class="form-control" id="cc_oc" name="cc_oc">
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label">Total Committed Cost (in million PhP)</label>
                                        <input type="number" class="form-control" id="cc_tcc" name="cc_tcc">
                                  </div>
                                  </div>
                                  <div class="row">
                                  <div class="form-group field col-6">                        
                                        <label class="control-label">Original currency</label>
                                        <select class="form-control ui dropdown" id="cc_currency" name="cc_currency">
                                        <option disabled="" selected="">Choose</option>
                                        @foreach ($currency as $currenc)
                                          <option id="{{ $currenc->id }}" value="{{ $currenc->id }}">{{ $currenc->currency }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group field col-6"> 
                                        <label class="control-label">Amount in Original Currency</label>
                                        <input type="number" class="form-control" id="cc_amount_orig_currency" name="cc_amount_orig_currency">
                                  </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="input-group col-12">
                                        <label class="control-label col-3 adjust">Currency to US$1 =</label>
                                        <input type="number" class="form-control col-2" id="cc_c_dol" name="cc_c_dol">
                                        <label class="control-label col-3 adjust">US$1 to PhP =</label>
                                        <input type="number" class="form-control col-2" id="cc_d_php" name="cc_d_php">
                                      </div>
                                  </div>
                                  <div class="form-group field">
                                    <label for="title">Remarks</label>
                                    <input type="text" class="form-control" id="cc_remarks" aria-describedby="cc_remarks" name="cc_remarks">
                                  </div>
                                  </div>
                              </div>
                            </div>
                          </div> -->
                      </div>
                    <!-- End of Financial History -->
                    <!-- Start of ICC Tracking -->
                      <br>
                      <div id="it">
                        <a class="ui purple image label">
                        III. 
                        <div class="detail">ICC TRACKING</div>
                      </a>
                        <div class="container">
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="form-group field">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="ICCable_switch" onclick="ICCSwitch()" name="ICCable_switch" value="1">
                                  <label class="custom-control-label" for="ICCable_switch"><b>ICC-able</b></label>
                                </div>
                                <div class="custom-control  custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="IFP_switch" name="IFP_switch" value="1">
                                  <label class="custom-control-label" for="IFP_switch"><b>IFP</b></label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="PIP_switch" onclick="PIPSwitcher()" name="PIP_switch" value="1">
                                  <label class="custom-control-label" for="PIP_switch"><b>Public Investment Program</b></label>
                                </div>
                                <div class="form-group field" id="pipcode_d" style="display: none;">
                                    <label class="control-label"><i class="flag icon blue"></i><b>PIPOL Code</b></label>
                                    <input type="text" class="form-control" id="pipolcode" name="pipolcode">
                                </div>
                                <div class="form-group field" id="currentlevel_icc" style="display: none;" >
                                <div class="form-group field">
                                  <label class="control-label"><i class="flag icon blue"></i><b>Current Level of ICC Review</b></label>
                                    <select class="form-control" id="currentlevel" name="currentlevel" onchange="java_script_:showNEDABoard(this.options[this.selectedIndex].value)">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($levels as $level_)
                                      <option id="{{ $level_->id }}" value="{{ $level_->id }}">{{ $level_->level }}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group field">
                                  <label for="estaff">ICC documentary requirements</label>
                                      <select class="form-control ui dropdown" id="docs" multiple="multiple" name="docs[]">
                                        <option disabled="" selected="">Choose</option>
                                        @foreach ($documents as $document_)
                                          <option id="{{ $document_->id }}" value="{{ $document_->id }}">{{ $document_->document }}</option>
                                        @endforeach
                                      </select>
                                </div>
                                </div>
                                <div class="form-group field" id="nb_approval" style="display: none;">
                                  <label class="control-label"><i class="flag icon blue"></i><b>NEDA Board Approved</b></label>
                                  <select class="form-control" id="nbapproval" name="nbapproval">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($nbapprovals as $nbapproval)
                                      <option id="{{ $nbapproval->id }}" value="{{ $nbapproval->id }}">{{ $nbapproval->approval_type }}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Dates</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <label class="control-label">Date of Proponent Agency Endorsement</label>
                                      <input type="date" class="form-control" aria-describedby="emailHelp" name="date_endorsement" id="date_endorsement">
                                  </div>
                                  <div class="form-group field">
                                      <label class="control-label">Date of Receipt of NEDA</label>
                                      <input type="date" class="form-control" aria-describedby="emailHelp" name="date_receipt" id="date_receipt">
                                  </div>
                                  <div class="form-group field">
                                      <label class="control-label">Date of PIS Referral to Evaluating Staff</label>
                                      <input type="date" class="form-control" aria-describedby="emailHelp" name="date_referral" id="date_referral">
                                  </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>ICC-TB Dates</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <div class="input-group col-12">
                                        <table class="form-table ui celled table" id="ICCTB">
                                          <tr valign="top">
                                            <td>
                                              Date
                                            </td>
                                            <td>
                                            En banc or ad referendum
                                            </td>
                                            <td>
                                              Action
                                            </td>
                                            <td>
                                            Material Used
                                            </td>
                                            <td>
                                              <a href="javascript:void(0);" class="addICCTB">Add</a>
                                            </td>
                                          </tr>
                                        </table>
                                      </div>
                                  </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>ICC-CC Dates</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <div class="input-group col-12">
                                        <table class="form-table ui celled table" id="ICCCC">
                                          <tr valign="top">
                                            <td>
                                              Date
                                            </td>
                                            <td>
                                            En banc or ad referendum
                                            </td>
                                            <td>
                                              Action
                                            </td>
                                            <td>
                                            Material Used
                                            </td>
                                            <td>
                                              <a href="javascript:void(0);" class="addICCCC">Add</a>
                                            </td>
                                          </tr>
                                        </table>
                                      </div>
                                  </div>
                                  </div>
                                  </div>
                              </div>
                            </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>NEDA Board Dates</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <div class="input-group col-12">
                                        <table class="form-table ui celled table" id="NBD">
                                          <tr valign="top">
                                            <td>
                                              Date
                                            </td>
                                            <td>
                                            En banc or ad referendum
                                            </td>
                                            <td>
                                              Action
                                            </td>
                                            <td>
                                            Material Used
                                            </td>
                                            <td>
                                              <a href="javascript:void(0);" class="addNBD">Add</a>
                                            </td>
                                          </tr>
                                        </table>
                                      </div>
                                  </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="form-group field">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="Reevaluation_switch" onclick="ReevaluationSwitch()" name="Reevaluation_switch" value="1">
                                  <label class="custom-control-label" for="Reevaluation_switch"><i class="flag icon blue"></i><b>For Reevaluation</b></label>
                                </div>
                                <div class="form-group field" id="reeval" style="display:none;" >
                                  <label class="control-label"><i class="flag icon blue"></i><b>Reason for Reevaluation</b></label>
                                          <select class="form-control" id="reasons" name="reasons" onchange="java_script_:showOthersReasons(this.options[this.selectedIndex].value)">
                                            <option disabled="" selected="" value="">Choose</option>
                                            @foreach ($reasons as $reason_)
                                              <option value="{{ $reason_->id }}" id="{{ $reason_->id }}">{{ $reason_->reason }}</option>
                                            @endforeach
                                          </select>
                                </div>
                                <div class="form-group field" id="others2" style="display: none;">
                                  <label class="control-label"><b>Others (please specify)</b></label>
                                  <input type="text" class="form-control" id="reason_others" name="reason_others">
                                </div>
                              </div>
                              <div class="form-group field">
                                  <label class="control-label"><i class="flag icon blue"></i><b>Agency Contact</b></label>
                                  <input type="text" class="form-control" id="agency_contact" name="agency_contact">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!-- End of ICC Tracking -->
                      <br>
                      <div id="fd">
                      <div id="ifoda" style="display: none;">
                        <a class="ui orange image label">
                        IV. 
                        <div class="detail">OFFICIAL DEVELOPMENT ASSISTANCE</div>
                      </a>
                        <div class="container">
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="form-group field">
                                  <label class="control-label"><i class="flag icon blue"></i><b>Mode of Assistance</b></label>
                                  <select class="form-control" id="oda_mode" name="oda_mode">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($modes as $mode_)
                                      <option id="{{ $mode_->id }}" value="{{ $mode_->id }}">{{ $mode_->mode }}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <div class="form-group field">
                                  <label class="control-label"><i class="flag icon blue"></i><b>Financing Status</b></label>
                                  <select class="form-control" id="oda_status" name="oda_status">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($status as $status_)
                                      <option id="{{ $status_->id }}" value="{{ $status_->id }}">{{ $status_->status }}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <div class="form-group field">
                                  <label class="control-label"><i class="flag icon blue"></i><b>Funding Institution</b></label>
                                  <select class="form-control" id="oda_funding" name="oda_funding">
                                    <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($fundings as $funding)
                                      <option id="{{ $funding->fsource_no }}" value="{{ $funding->fsource_no }}">{{ $funding->fsource_description }}</option>
                                    @endforeach
                                  </select>
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-6">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Loan Agreement Signing</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" class="custom-control-input" name="loan_agreement" value="1">
                                        <label class="custom-control-label" for="customRadio1">Date Signed</label>
                                        <input type="date" class="form-control" id="loan_agreement_date" name="loan_agreement_date">
                                      </div>
                                      <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio2" name="loan_agreement" class="custom-control-input" value="2">
                                        <label class="custom-control-label" for="customRadio2">Yet to be Signed</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <div class="col-6">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Loan Effectivity</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                      <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio3" name="loan_effectiveness" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="customRadio3">Loan Effectiveness Date</label>
                                         <input type="date" class="form-control" id="loan_effectiveness_date" name="loan_effectiveness_date">
                                      </div>
                                      <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio4" class="custom-control-input" name="loan_effectiveness" value="2">
                                        <label class="custom-control-label" for="customRadio4">Not Yet Effective</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="card border-secondary">
                                <div class="card-header"><b>Implementation Period</b></div>
                                <div class="card-body">
                                  <div class="form-group field">
                                    <label class="control-label"><i class="flag icon blue"></i>Implementation Duration (in number of months)</label>
                                    <input type="number" class="form-control" id="loan_im_date_start" name="loan_im_date_start"> 
                                  </div>
                                  <div class="form-group field">
                                    <label class="control-label"><i class="flag icon blue"></i>Remarks</label>
                                    <input type="text" class="form-control" id="loan_im_remarks" name="loan_im_remarks">
                                  </div>
                                </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      <div id="ifppp" style="display: none;">
                         <a class="ui orange image label">
                        IV.
                        <div class="detail">PUBLIC PRIVATE PARTNERSHIP</div>
                        </a>
                        <div class="container">
                          <br>
                          <div class="row">
                            <div class="col-6">
                              <div class="form-group field">
                                  <label class="control-label"><b>Grantor</b></label>
                                  <select class="form-control ui" id="ppp_grantor" name="ppp_grantor">
                                      <option disabled="" selected="" value="">Choose</option>
                                    @foreach ($agencies as $agency)
                                      <option id="{{ $agency->id }}" value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                    @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-6">
                              <div class="form-group field">
                                  <label class="control-label"><b>Original Proponent for Unsolicited Proposals</b></label>
                                   <input type="text" class="form-control" id="ppp_orig" aria-describedby="ppp_orig" name="ppp_orig">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-6">
                              <div class="form-group field">
                                  <label class="control-label"><b>Date Granted of OPS</b></label>
                                  <input type="date" class="form-control" id="ppp_date_granted" aria-describedby="ppp_date_granted" name="ppp_date_granted">
                              </div>
                            </div>
                            <div class="col-3">

                              <div class="form-group field">
                                  <label class="control-label"><b>Concession Period</b></label>
                                   <input type="month" class="form-control" id="ppp_year1" aria-describedby="ppp_year1" name="ppp_year1">
                              </div>
                            </div>
                            <div class="col-3">
                              <div class="form-group field">
                                <label class="control-label">&nbsp;</label>
                                   <input type="month" class="form-control" id="ppp_year2" aria-describedby="ppp_year2" name="ppp_year2">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-6">
                              <div class="form-group field">
                                  <label class="control-label"><b>Bid Parameter</b></label>
                                  <input type="text" class="form-control" id="ppp_bid" aria-describedby="ppp_bid" name="ppp_bid">
                              </div>
                            </div>
                            <div class="col-6">
                              <div class="form-group field">
                                  <label class="control-label"><b>Proposed ROR</b></label>
                                   <input type="text" class="form-control" id="ppp_ror" aria-describedby="ppp_ror" name="ppp_ror">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      </div>
                      <br>
                      <div id="coe">
                        <a class="ui red image label">
                        V.
                        <div class="detail">CHRONOLOGY OF EVENTS</div>
                      </a>
                        <div class="container">
                          <br>
                          <div class="row">
                            <div class="col-12">
                              <div class="form-group">
                              <table class="form-table ui celled table" id="Chrono">
                                <tr valign="top">
                                  <td colspan="3">
                                    <center>
                                    <a href="javascript:void(0);" class="addChrono">Add</a>
                                    </center>
                                  </td>
                                </tr>
                              </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>

                </div>
            </div>
        </div>

<script>

$("#main_form").validate({
  rules: {
    project_title: "required",
    objectives: "required",
    description: "required",
    status: "required",
    talkpoints: "required",
    iagency: "required",
    sfinancing: "required",
    sector: "required",
    "subsector[]": "required",
    impdate: "required",
    impdate2: "required",
    spatial: "required",
    ec_tec: "required",
    // oc_f: "required",
    // oc_currency: "required",
    // oc_amount_orig_currency: "required",
    // date_endorsement: "required",
    // date_referral: "required",
    // date_receipt: "required",
    agency_contact: "required",
    // compound rule
    oda_type: {
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        }
    },
    finance_others:{
      required: function(element) {
                              return $('#sfinancing').val() == '4';
                        }      
    },
    currentlevel:{
      required: function(element) {
                              return $('#ICCable_switch').val() == '1';
                        }      
    },
    pipolcode:{
      required: function(element) {
                              return $('#PIP_switch').val() == '1';
                        } 
    },
    reasons:{
      required: function(element) {
                              return $('#Reevaluation_switch').val() == '1';
                        } 
    },
    ppp_variance:{
      required: function(element) {
                              return $('#ppp_type').val() == '1' || $('#ppp_type').val() == '2';
                        } 

    },
    oda_mode:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    oda_status:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    oda_funding:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    loan_im_date_start:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    loan_im_date_end:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    loan_im_remarks:{
      required: function(element) {
                              return $('#sfinancing').val() == '1';
                        } 

    },
    ppp_others:{
      required: function(element) {
                              return $('#ppp_variance').val() == '11';
                        } 

    },
    reason_others:{
      required: function(element) {
                              return $('#reasons').val() == '6';
                        } 

    },
    nbapproval:{
      required: function(element) {
                              return $('#currentlevel').val() == '8';
                        } 

    },
  }
});

</script>

  <script>
    $(document).ready(function(){

      $(".addChrono").click(function(){
        $("#Chrono").append('<tr valign="top"><td>Date: <input type="date" class="code form-control" id="ChronologyDate" name="ChronologyDate[]"/></td><td>Remarks <input type="text" class="code form-control" id="ChronologyRemarks" name="ChronologyRemarks[]" value=""/></td><td><a href="javascript:void(0);" class="remChrono">Remove</a></td></tr>');
      });
        $("#Chrono").on('click','.remChrono',function(){
            $(this).parent().parent().remove();
        });


      $(".addICCTB").click(function(){
        $("#ICCTB").append('<tr valign="top"><td><input type="date" class="form-control" aria-describedby="emailHelp" id="ICCTBDate" name="ICCTBDate[]"></td><td><select class="form-control ui dropdown" id="ICCTBEAR" name="ICCTBEAR[]"><option disabled="" selected="">Choose</option><option id="1" value="En Banc">En Banc</option><option id="2" value="Ad Referendum">Ad Referendum</option></td><td><select class="form-control ui dropdown" id="ICCTBAction" name="ICCTBAction[]"><option disabled="" selected="">Choose</option>@foreach ($actions as $action_)<option id="{{ $action_->id }}" value="{{ $action_->id }}">{{ $action_->action }}</option>{{ $action_->action }}</option>@endforeach</select></td><td><select class="form-control ui dropdown" id="ICCTBMU" name="ICCTBMU[]"><option disabled="" selected="">Choose</option><option id="1" value="Project Evaluation Report">Project Evaluation Report</option><option id="2" value="Preliminary Report">Preliminary Report</option><option id="3" value="Memorandum to the ICC">Memorandum to the ICC</option><option id="4" value="Others (please specify)​">Others (please specify)​</option></td><td><a href="javascript:void(0);" class="remICCTB">Remove</a></td></tr>');
      });
        $("#ICCTB").on('click','.remICCTB',function(){
            $(this).parent().parent().remove();
        });

      $(".addICCCC").click(function(){
        $("#ICCCC").append('<tr valign="top"><td><input type="date" class="form-control" aria-describedby="emailHelp" id="ICCCCDate" name="ICCCCDate[]"></td><td><select class="form-control ui dropdown" id="ICCCCEAR" name="ICCCCEAR[]"><option disabled="" selected="">Choose</option><option id="1" value="En Banc">En Banc</option><option id="2" value="Ad Referendum">Ad Referendum</option></td><td><select class="form-control ui dropdown" id="ICCCCAction" name="ICCCCAction[]"><option disabled="" selected="">Choose</option>@foreach ($actions as $action_)<option id="{{ $action_->id }}" value="{{ $action_->id }}">{{ $action_->action }}</option>{{ $action_->action }}</option>@endforeach</select></td><td><select class="form-control ui dropdown" id="ICCCCMU" name="ICCCCMU[]"><option disabled="" selected="">Choose</option><option id="1" value="Project Evaluation Report">Project Evaluation Report</option><option id="2" value="Preliminary Report">Preliminary Report</option><option id="3" value="Memorandum to the ICC">Memorandum to the ICC</option><option id="4" value="Others (please specify)​">Others (please specify)​</option></td><td><a href="javascript:void(0);" class="remICCCC">Remove</a></td></tr>');
      });
        $("#ICCCC").on('click','.remICCCC',function(){
            $(this).parent().parent().remove();
        });

      $(".addNBD").click(function(){
        $("#NBD").append('<tr valign="top"><td><input type="date" class="form-control" aria-describedby="emailHelp" id="NBDDate" name="NBDDate[]"></td><td><select class="form-control ui dropdown" id="NBDEAR" name="NBDEAR[]"><option disabled="" selected="">Choose</option><option id="1" value="En Banc">En Banc</option><option id="2" value="Ad Referendum">Ad Referendum</option></td><td><select class="form-control ui dropdown" id="NBDAction" name="NBDAction[]"><option disabled="" selected="">Choose</option>@foreach ($actions as $action_)<option id="{{ $action_->id }}" value="{{ $action_->id }}">{{ $action_->action }}</option>{{ $action_->action }}</option>@endforeach</select></td><td><select class="form-control ui dropdown" id="NBDMU" name="NBDMU[]"><option disabled="" selected="">Choose</option><option id="1" value="Project Evaluation Report">Project Evaluation Report</option><option id="2" value="Preliminary Report">Preliminary Report</option><option id="3" value="Memorandum to the ICC">Memorandum to the ICC</option><option id="4" value="Others (please specify)​">Others (please specify)​</option></td><td><a href="javascript:void(0);" class="remNBD">Remove</a></td></tr>');
      });
        $("#NBD").on('click','.remNBD',function(){
            $(this).parent().parent().remove();
        });
    });
  </script>

<!-- IF SCRIPTS -->
  <script>
    function showSourceOfFinancing(sfinancing) {
      if (sfinancing == "1") {
        oda2.style.display='block';
        ifoda.style.display='block';
        ifppp.style.display='none';
        ppp2.style.display='none';
        others1.style.display='none';
        ppp_var.style.display='none';
        others3.style.display='none';
      } else if (sfinancing == "3") {
        oda2.style.display='none';
        ifoda.style.display='none';
        ifppp.style.display='block';
        ppp2.style.display='block';
        others1.style.display='none';
      } else if (sfinancing == "4") {
        oda2.style.display='none';
        ifoda.style.display='none';
        ifppp.style.display='none';
        ppp2.style.display='none';
        others1.style.display='block';
        ppp_var.style.display='none';
        others3.style.display='none';
      } else {
        oda2.style.display='none';
        ifoda.style.display='none';
        ifppp.style.display='none';
        ppp2.style.display='none';
        others1.style.display='none';
        ppp_var.style.display='none';
        others3.style.display='none';
      }
    }

    function showSubsector(sector) {
      if (sector=="1") {
        sector_1.style.display='block';
        sector_2.style.display='none';
        sector_3.style.display='none';
        sector_4.style.display='none';
        sector_5.style.display='none';
      } else if (sector=="2") {
        sector_1.style.display='none';
        sector_2.style.display='block';
        sector_3.style.display='none';
        sector_4.style.display='none';
        sector_5.style.display='none';
      } else if (sector=="3") {
        sector_1.style.display='none';
        sector_2.style.display='none';
        sector_3.style.display='block';
        sector_4.style.display='none';
        sector_5.style.display='none';
      } else if (sector=="4") {
        sector_1.style.display='none';
        sector_2.style.display='none';
        sector_3.style.display='none';
        sector_4.style.display='block';
        sector_5.style.display='none';
      } else if (sector=="5") {
        sector_1.style.display='none';
        sector_2.style.display='none';
        sector_3.style.display='none';
        sector_4.style.display='none';
        sector_5.style.display='block';
      }
    }

    function showPPPVariance(ppp) {
      if ((ppp == "1" || ppp == "2")) {
        ppp_var.style.display='block';
        ppp_type_others_field.style.display='none';
      } else if (ppp == "4") {
        ppp_var.style.display='none';
        ppp_type_others_field.style.display='block';
      } else {
        ppp_var.style.display='none';
        ppp_type_others_field.style.display='none';
      }
    }

    function showSpatial(spatial) {
      if (spatial == "2") {
        if_inter.style.display='block';
        if_reg.style.display='none';
      } else if (spatial == "3") {
        if_reg.style.display='block';
        if_inter.style.display='none';
      } else {
        if_inter.style.display='none';
        if_reg.style.display='none';
      }
    }

    function showOthersReasons(reasons) {
      if (reasons == "6") {
        others2.style.display='block';
      } else {
        others2.style.display='none';
      }
    }

    function showOtherVariance(ppp_variance) {
      if (ppp_variance == "11") {
        others3.style.display='block';
      } else {
        others3.style.display='none';
      }
    }

      function showNEDABoard(currentlevel) {
      if (currentlevel == "8") {
        nb_approval.style.display='block';
      } else {
        nb_approval.style.display='none';
      }
      
    }


  </script> 
              

  <!-- CHECKBOXES -->
  <script type="text/javascript">
  function ICCSwitch() {
      var checkBox = document.getElementById("ICCable_switch");
      var text = document.getElementById("currentlevel_icc");

      if (checkBox.checked == true){
        text.style.display = "block";

      } else {
        text.style.display = "none";
      }
  }

  function PIPSwitcher() {
      var checkBox = document.getElementById("PIP_switch");
      var text = document.getElementById("pipcode_d");

      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
  }

 function ReevaluationSwitch() {
      var checkBox = document.getElementById("Reevaluation_switch");
      var text = document.getElementById("reeval");

      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
  }
  </script>  

@endsection


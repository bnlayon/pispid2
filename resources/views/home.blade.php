@extends('layouts.app')

@section('content')
@if(Auth::user()->active == 0)
  <meta http-equiv="refresh" content="0;url=http://localhost:8000/logout" />
@endif
        <div class="col-12">

            <div class="jumbotron">
              <h1 class="display-3">Welcome to PIS-PPID!</h1>
              <p class="lead">
                <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
              </p>
            </div>

            <div class="row">
                <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">My Dashboard</li>
                </ol>
                </div>
            </div>
            
            <div class="row">
<iframe width="600" height="373.5" src="https://app.powerbi.com/view?r=eyJrIjoiNDQ3M2VlNjYtNjU1Ny00NjUyLWFlMzktM2IxMDQxYjVlNzZhIiwidCI6IjUyYzA4NGNjLWNkMTUtNDY3MS04YTU3LWMxOTU2NWJjZGZjMiIsImMiOjEwfQ%3D%3D&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>
            </div>
            <br>

        </div>

@endsection


@extends('layouts.app')

@section('content')
@if(Auth::user()->active == 0 && Auth::user()->role != 0)
  <meta http-equiv="refresh" content="1;url=http://localhost:8000/logout" />
@endif

  @if (Auth::user()->role == 'AD')
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Administration</li>
                </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                  <table id="users" class="display ui striped table" style="width:100%">
                    <thead>
                            <tr>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                    </thead>
                    <tbody>
                      @foreach ($users as $user)
                            <tr>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->role }}</td>
                              <td>
                                @if($user->active == 1 && $user-> role != 'AD')
                                  <form action="{{ asset('/deactivate') }}/{{$user->id}}" method="POST" class="form ui" novalidate="">{{ csrf_field() }}
                                  <button class="mini warning submit ui button">Deactivate</button>
                                  </form>
                                @else
                                  @if($user-> role != 'AD')
                                <form action="{{ asset('/activate') }}/{{$user->id}}" method="POST" class="form ui" novalidate="">{{ csrf_field() }}
                                  <button class="mini positive submit ui button">Activate</button>
                                </form>
                                  @else

                                  @endif
                                @endif
                                  <button onclick="document.getElementById('id{{ $user->id }}').style.display='block'" class="mini blue ui button">View Logs</button>
                                  <div id="id{{ $user->id }}" class="w3-modal w3-animate-opacity" style="top: -50px;">
                                    <div class="w3-modal-content">
                                      <div class="w3-container ">
                                        <br>
                                        <span onclick="document.getElementById('id{{ $user->id }}').style.display='none'" class="w3-button w3-display-topright"><i class="window close icon"></i></span>
                                          <div class="column">
                                            <div class="ui raised segment">
                                            </div>
                                            <br>
                                          </div>
                                          <br>
                                      </div>
                                    </div>
                                  </div>
                              </td>
                            </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>

  @endif

      

@endsection


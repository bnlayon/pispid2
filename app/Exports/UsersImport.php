<?php

namespace App\Exports;

use App\Users;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersImport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Users::all();
    }
}

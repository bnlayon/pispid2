<?php

namespace App\Exports;

use App\Projects;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProjectExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $source, $IFP_switch, $iagency, $currentlevel, $field;

 function __construct($source, $IFP_switch, $iagency, $currentlevel, $field, $PIP_switch) {
        $this->source = $source;
        $this->IFP_switch = $IFP_switch;
        $this->iagency = $iagency;
        $this->currentlevel = $currentlevel;
        $this->field = $field;
        $this->PIP_switch = $PIP_switch;
 }


public function headings(): array
    {
        if(!empty($this->field)){
        return [
            $this->field
        ];
        }else{
        return [
            'id',
            'project_title',
            'created_at',
            'updated_at',
            'objectives',
            'description',
            'status',
            'iagency',
            'sfinancing',
            'sector',
            'estafflead',
            'impdate',
            'aagency',
            'spatial',
            'region',
            'cost_fc',
            'cost_lc',
            'cost_pc',
            'cost_tpc',
            'oc_f',
            'oc_tpc',
            'oc_currency',
            'oc_amount_orig_currency',
            'oc_c_dol',
            'oc_d_php',
            'ec_f',
            'ec_gdp',
            'ec_oc',
            'ec_tec',
            'ec_currency',
            'ec_amount_orig_currency',
            'ec_c_dol',
            'ec_d_php',
            'cc_f',
            'cc_gdp',
            'cc_oc',
            'cc_tcc',
            'cc_currency',
            'cc_amount_orig_currency',
            'cc_c_dol',
            'cc_d_php',
            'ICCable_switch',
            'IFP_switch',
            'PIP_switch',
            'pipolcode',
            'currentlevel',
            'date_endorsement',
            'date_receipt',
            'date_referral',
            'oda_type',
            'ppp_type',
            'ppp_variance',
            'finance_others',
            'ppp_others',
            'Reevaluation_switch',
            'reasons',
            'reason_others',
            'agency_contact',
            'oda_mode',
            'oda_status',
            'oda_funding',
            'loan_agreement',
            'loan_agreement_date',
            'loan_effectiveness',
            'loan_effectiveness_date',
            'loan_im_date_start',
            'loan_im_remarks',
            'nbapproval',
            'agency_focal',
            'agency_number',
            'agency_email',
            'ppp_type_others',
            'es_division',
            'impdate2',
            'oc_remarks',
            'ec_remarks',
            'cc_remarks',
            'ppp_grantor',
            'ppp_orig',
            'ppp_date_granted',
            'ppp_year1',
            'ppp_year2',
            'ppp_bid',
            'ppp_ror'
        ];
        }
    }

public function collection()
    {
       $query = \App\Projects::query();

       $query = $query->join('agencies', 'projects.iagency', '=', 'agencies.id')->leftJoin('sources', 'projects.sfinancing', '=', 'sources.id')->leftJoin('sectors', 'projects.sector', '=', 'sectors.id')->leftJoin('evaluatingstaffs', 'projects.estafflead', '=', 'evaluatingstaffs.id')->leftJoin('spatials', 'projects.spatial', '=', 'spatials.id');

       if(!is_null($this->iagency)){
        	$query = $query->where('projects.iagency', $this->iagency);
        }

        if(!is_null($this->source)){
        	$query = $query->where('projects.sfinancing', $this->source);
        }

        if(!is_null($this->IFP_switch)){
        	$query = $query->where('projects.IFP_switch', $this->IFP_switch);
        }

        if(!is_null($this->PIP_switch)){
            $query = $query->where('projects.PIP_switch', $this->PIP_switch);
        }

		if(!is_null($this->currentlevel)){
        	$query = $query->where('projects.currentlevel', $this->currentlevel);
        }
        // where('projects.sfinancing', $this->source)
        // 					->where('projects.IFP_switch', $this->IFP_switch)


        if(!is_null($this->field)){
            foreach ($this->field as $key => $value) {
                if($value == 'iagency'){
                    $query = $query->addSelect('agencies.Abbreviation');
                }elseif($value == 'sfinancing'){
                    $query = $query->addSelect('sources.type');
                }elseif($value == 'sector'){
                    $query = $query->addSelect('sectors.sector');
                }elseif($value == 'estafflead'){
                    $query = $query->addSelect('evaluatingstaffs.abbrv');
                }elseif($value == 'spatial'){
                    $query = $query->addSelect('spatials.spatial');
                }else{
                    $query = $query->addSelect($value);
                }
            }
            
        }else{
            $query = $query->addSelect('projects.id',
            'project_title',
            'created_at',
            'updated_at',
            'objectives',
            'description',
            'status',
            'agencies.Abbreviation',
            'sources.type',
            'sectors.sector',
            'evaluatingstaffs.abbrv',
            'impdate',
            'aagency',
            'spatials.spatial',
            'region',
            'cost_fc',
            'cost_lc',
            'cost_pc',
            'cost_tpc',
            'oc_f',
            'oc_tpc',
            'oc_currency',
            'oc_amount_orig_currency',
            'oc_c_dol',
            'oc_d_php',
            'ec_f',
            'ec_gdp',
            'ec_oc',
            'ec_tec',
            'ec_currency',
            'ec_amount_orig_currency',
            'ec_c_dol',
            'ec_d_php',
            'cc_f',
            'cc_gdp',
            'cc_oc',
            'cc_tcc',
            'cc_currency',
            'cc_amount_orig_currency',
            'cc_c_dol',
            'cc_d_php',
            'ICCable_switch',
            'IFP_switch',
            'PIP_switch',
            'pipolcode',
            'currentlevel',
            'date_endorsement',
            'date_receipt',
            'date_referral',
            'oda_type',
            'ppp_type',
            'ppp_variance',
            'finance_others',
            'ppp_others',
            'Reevaluation_switch',
            'reasons',
            'reason_others',
            'agency_contact',
            'oda_mode',
            'oda_status',
            'oda_funding',
            'loan_agreement',
            'loan_agreement_date',
            'loan_effectiveness',
            'loan_effectiveness_date',
            'loan_im_date_start',
            'loan_im_remarks',
            'nbapproval',
            'agency_focal',
            'agency_number',
            'agency_email',
            'ppp_type_others',
            'es_division',
            'impdate2',
            'oc_remarks',
            'ec_remarks',
            'cc_remarks',
            'ppp_grantor',
            'ppp_orig',
            'ppp_date_granted',
            'ppp_year1',
            'ppp_year2',
            'ppp_bid',
            'ppp_ror');
        }

// $scrapper->prependRow(array(
//     'prepended', 'prepended'
// ));

        $a = $query->get();

        return $scrapper = $a;
    }


}

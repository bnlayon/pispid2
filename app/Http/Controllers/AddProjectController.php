<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;

class AddProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addproject()
    {
        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC')->get();
        $sources = \App\Sources::all();
        $loantypes = \App\Loantypes::all();
        $ppptypes = \App\Ppptypes::all();
        $sectors = \App\Sectors::all();
        $subsectors = \App\Subsectors::all();
        $leadevaluatingstaffs = \App\Evaluatingstaffs::where('lead', 1)->get();
        $evaluatingstaffs = \App\Evaluatingstaffs::all();
        $spatials = \App\Spatial::all();
        $regions = \App\Regions::all();
        $currency = \App\Currency::all();
        $levels = \App\Iccapprovals::all();
        $actions = \App\Actions::all();
        $reasons = \App\Reasons::all();
        $modes = \App\Modes::all();
        $status = \App\Financingstatus::all();
        $fundings = \App\Fundings::all();
        $variances = \App\Variances::all();
        $nbapprovals = \App\Nbapprovals::all();
        $documents = \App\Documents::all();
        return view('addproject', compact('agencies', 'sources', 'loantypes', 'ppptypes', 'sectors', 'subsectors', 'leadevaluatingstaffs', 'evaluatingstaffs','spatials','regions', 'currency', 'levels', 'actions', 'reasons','modes', 'status', 'fundings', 'variances','nbapprovals','documents'));
    }

    public function saveproject(Request $request){
        $project                             = new \App\Projects();
        $project->project_title              = request('project_title');
        $project->objectives                 = request('objectives');
        $project->description                = request('description');
        $project->status                     = request('status');
        $project->talkpoints                 = request('talkpoints');
        $project->iagency                    = request('iagency');
        $project->aagency                    = request('aagency');
        $project->sfinancing                 = request('sfinancing');
        $project->sector                     = request('sector');
        $project->aagency                    = request('aagency');
        $project->sfinancing                 = request('sfinancing');
        $project->sector                     = request('sector');
        $project->estafflead                 = request('estafflead');
        $project->impdate                    = request('impdate');
        $project->impdate2                   = request('impdate2');
        $project->spatial                    = request('spatial');
        $project->region                     = request('region');       
        // $project->cost_fc                    = request('cost_fc');
        // $project->cost_pc                    = request('cost_pc');
        // $project->cost_lc                    = request('cost_lc');
        // $project->cost_tpc                   = request('cost_tpc');
        // $project->oc_f                       = request('oc_f');
        // $project->oc_tpc                     = request('oc_tpc');
        // $project->oc_currency                = request('oc_currency');
        // $project->oc_amount_orig_currency    = request('oc_amount_orig_currency');
        // $project->oc_c_dol                   = request('oc_c_dol');
        // $project->oc_d_php                   = request('oc_d_php');
        $project->ec_f                       = request('ec_f');
        $project->ec_gdp                     = request('ec_gdp');
        $project->ec_oc                      = request('ec_oc');
        $project->ec_tec                     = request('ec_tec');
        $project->ec_currency                = request('ec_currency');
        $project->ec_amount_orig_currency    = request('ec_amount_orig_currency');
        $project->ec_d_php                   = request('ec_d_php');
        $project->ec_c_dol                   = request('ec_c_dol');
        // $project->cc_f                       = request('cc_f');
        // $project->cc_gdp                     = request('cc_gdp');
        // $project->cc_oc                      = request('cc_oc');
        // $project->cc_tcc                     = request('cc_tcc');
        // $project->cc_currency                = request('cc_currency');
        // $project->cc_amount_orig_currency    = request('cc_amount_orig_currency');
        // $project->cc_d_php                   = request('cc_d_php');
        // $project->cc_c_dol                   = request('cc_c_dol');
        $project->ICCable_switch             = request('ICCable_switch');
        $project->IFP_switch                 = request('IFP_switch');
        $project->PIP_switch                 = request('PIP_switch');
        $project->pipolcode                  = request('pipolcode');
        $project->currentlevel               = request('currentlevel');
        $project->date_endorsement           = request('date_endorsement');
        $project->date_receipt               = request('date_receipt');
        $project->date_referral              = request('date_referral');
        $project->oda_type                   = request('oda_type');
        $project->ppp_type                   = request('ppp_type');
        $project->ppp_variance               = request('ppp_variance');
        $project->finance_others             = request('finance_others');
        $project->ppp_others                 = request('ppp_others');
        $project->Reevaluation_switch        = request('Reevaluation_switch');
        $project->reasons                    = request('reasons');
        $project->reason_others              = request('reason_others');
        $project->agency_contact             = request('agency_contact');
        $project->oda_mode                   = request('oda_mode');
        $project->oda_status                 = request('oda_status');
        $project->oda_funding                = request('oda_funding');
        $project->loan_agreement             = request('loan_agreement');
        $project->loan_agreement_date        = request('loan_agreement_date');
        $project->loan_effectiveness         = request('loan_effectiveness');
        $project->loan_effectiveness_date    = request('loan_effectiveness_date');
        $project->loan_im_date_start         = request('loan_im_date_start');
        // $project->loan_im_date_end           = request('loan_im_date_end');
        $project->loan_im_remarks            = request('loan_im_remarks');
        $project->nbapproval                 = request('nbapproval');
        $project->agency_focal               = request('agency_focal');
        $project->agency_email               = request('agency_email');
        $project->agency_number              = request('agency_number');
        $project->ppp_type_others            = request('ppp_type_others');
        $project->es_division                = request('es_division');
        // $project->oc_remarks                 = request('oc_remarks');     
        $project->ec_remarks                 = request('ec_remarks');     
        // $project->cc_remarks                 = request('cc_remarks');   
        $project->ppp_grantor                = request('ppp_grantor');
        $project->ppp_orig                   = request('ppp_orig');
        $project->ppp_date_granted           = request('ppp_date_granted');
        $project->ppp_year1                  = request('ppp_year1');     
        $project->ppp_year2                  = request('ppp_year2');     
        $project->ppp_bid                    = request('ppp_bid');   
        $project->ppp_ror                    = request('ppp_ror');   
        $project->save();

        if(!is_null(request('docs'))){
            $docs_to_saves = request('docs');
            foreach ($docs_to_saves as $docs_to_save) {
                $project_docs                       = new \App\Projectdocuments();
                $project_docs->proj_id              = $project->id;
                $project_docs->doc_id               = $docs_to_save;
                $project_docs->save();
            }
        }

        if(!is_null(request('subsector'))){
            $subsector_to_saves = request('subsector');
            foreach ($subsector_to_saves as $subsector_to_save) {
                $project_subsector                       = new \App\Subsectorprojects();
                $project_subsector->proj_id              = $project->id;
                $project_subsector->subsector_id         = $subsector_to_save;
                $project_subsector->save();
            }
        }

        if(!is_null(request('estaff'))){
            $estaff_to_saves = request('estaff');
            foreach ($estaff_to_saves as $estaff_to_save) {
                $project_estaff                       = new \App\Evaluatingstaffsprojects();
                $project_estaff->proj_id              = $project->id;
                $project_estaff->staff_id             = $estaff_to_save;
                $project_estaff->save();
            }
        }

        if(!is_null(request('osectors'))){
            $osector_to_saves = request('osectors');
            foreach ($osector_to_saves as $osector_to_save) {
                $project_osector                       = new \App\Othersectors();
                $project_osector->proj_id              = $project->id;
                $project_osector->sector_id            = $osector_to_save;
                $project_osector->save();
            }
        }

        if(!is_null(request('interregional'))){
            $regions_to_saves = request('interregional');
            foreach ($regions_to_saves as $regions_to_save) {
                $project_region                     = new \App\Regionprojects();
                $project_region->proj_id            = $project->id;
                $project_region->region_id          = $regions_to_save;
                $project_region->save();
            }
        }

        if(!is_null(request('coiagency'))){
            $coiagency_to_saves = request('coiagency');
            foreach ($coiagency_to_saves as $coiagency_to_save) {
                $project_coiagency                       = new \App\Coimpprojects();
                $project_coiagency->proj_id              = $project->id;
                $project_coiagency->coimp_id             = $coiagency_to_save;
                $project_coiagency->save();
            }
        }

        $icctb_dates = request('ICCTBDate');
        $icctb_actions = request('ICCTBAction');
        $icctb_ear = request('ICCTBEAR');
        $icctb_materials = request('ICCTBMU');

        if(!empty($icctb_dates)){
            $count_ids = count(request('ICCTBDate'));
      
            for($i = 0; $i < $count_ids; $i++){
                $project_icctbdates = new \App\Icctbdatesprojects();
                $project_icctbdates->proj_id = $project->id;
                $project_icctbdates->date = $icctb_dates[$i];
                $project_icctbdates->remarks = $icctb_actions[$i];
                $project_icctbdates->ear = $icctb_ear[$i];
                $project_icctbdates->materials = $icctb_materials[$i];
                $project_icctbdates->save();
            } 
        }

        $icccc_dates = request('ICCCCDate');
        $icccc_actions = request('ICCCCAction');
        $icccc_ear = request('ICCCCEAR');
        $icccc_materials = request('ICCCCMU');

        if(!empty($icccc_dates)){
        $count_ids2 = count(request('ICCCCDate'));
            for($i = 0; $i < $count_ids2; $i++){
                $project_iccccdates = new \App\Iccccdatesprojects();
                $project_iccccdates->proj_id = $project->id;
                $project_iccccdates->date = $icccc_dates[$i];
                $project_iccccdates->remarks = $icccc_actions[$i];
                $project_iccccdates->ear = $icccc_ear[$i];
                $project_iccccdates->materials = $icccc_materials[$i];
                $project_iccccdates->save();
            }
        }

        $nb_dates = request('NBDDate');
        $nb_actions = request('NBDAction');
        $nb_ear = request('NBDEAR');
        $nb_materials = request('NBDMU');


        if(!empty($nb_dates)){
            $count_ids3 = count(request('NBDDate'));
      
            for($i = 0; $i < $count_ids3; $i++){
                $project_nbdates = new \App\Nbdatesprojects();
                $project_nbdates->proj_id = $project->id;
                $project_nbdates->date = $nb_dates[$i];
                $project_nbdates->remarks = $nb_actions[$i];
                $project_nbdates->ear = $nb_ear[$i];
                $project_nbdates->materials = $nb_materials[$i];
                $project_nbdates->save();
            }
        }

        $chrono_dates = request('ChronologyDate');
        $chrono_actions = request('ChronologyRemarks');

        if(!empty($chrono_dates)){
        $count_ids4 = count(request('ChronologyDate'));
  
            for($i = 0; $i < $count_ids4; $i++){
                $project_chronodates = new \App\Chronodatesprojects();
                $project_chronodates->proj_id = $project->id;
                $project_chronodates->date = $chrono_dates[$i];
                $project_chronodates->remarks = $chrono_actions[$i];
                $project_chronodates->save();
            }
        }

        $logs            = new \App\Systemlogs();
        $logs->ip        = request()->ip();
        $logs->proj_id   = $project->id;
        $logs->username  = Auth::user()->name;
        $logs->user_id  = Auth::user()->id;
        $logs->log       = "Added Project";

        $logs->save();

        alert()->success('Project Saved!')->persistent("Close");
        return back();
    }
}

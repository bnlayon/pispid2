<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewproject()
    {
        $projects = \App\Projects::leftJoin('agencies', 'projects.iagency', '=', 'agencies.id')
                                    ->leftJoin('sources', 'projects.sfinancing', '=', 'sources.id')
                                    ->leftJoin('iccapprovals', 'projects.currentlevel', '=', 'iccapprovals.id')
                                    ->select('projects.id', 'projects.project_title','projects.iagency','projects.status', 'agencies.UACS_AGY_DSC', 'projects.cost_tpc',
                                        'sources.type', 'iccapprovals.level', 'projects.status', 'projects.talkpoints')
                                    ->get();
        return view('viewproject', compact('projects'));
    }

    // public function getChronoDates($id){
    //     $project_events = \App\Chronodatesprojects::join('projects', 'projects.id', '=', 'chronodatesprojects.proj_id')
    //                                 ->select('chronodatesprojects.date','chronodatesprojects.remarks')
    //                                 ->where('chronodatesprojects.proj_id', '=', $id)
    //                                 ->get();
    //     return $project_events;
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LoginUserController extends Controller
{
    public function logout() {
        auth()->logout();
        Session::flush();
        return redirect('/');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\ProjectExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Schema;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report()
    {
        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC')->get();
        $sources = \App\Sources::all();
        $levels = \App\Iccapprovals::all();

        $fields = Schema::getColumnListing((new \App\Projects())->getTable());
     // print_r($fields);
        return view('report', compact('sources', 'agencies','levels','fields'));
    }

    
    public function importExportView()
    {
       return view('import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */

    public function export() 
    {
        return Excel::download(new UsersExport, 'masterlist.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function import() 
    {
        Excel::import(new UsersImport,request()->file('file'));
           
        return back();
    }

    public function export_filter() 
    {

        if(!empty(request('field'))){
            $field = request('field');
        }else{
            $field = null;
        }

        if(!empty(request('source'))){
            $source = request('source');
        }else{
            $source = null;
        }

        if(!empty(request('IFP_switch'))){
            $IFP_switch = request('IFP_switch');
        }else{
            $IFP_switch = null;
        }

        if(!empty(request('PIP_switch'))){
            $PIP_switch = request('PIP_switch');
        }else{
            $PIP_switch = null;
        }

        if(!empty(request('iagency'))){
            $iagency = request('iagency');
        }else{
            $iagency = null;
        }

        if(!empty(request('currentlevel'))){
            $currentlevel = request('currentlevel');
        }else{
            $currentlevel = null;
        }
        
        return Excel::download(new ProjectExport($source, $IFP_switch, $iagency, $currentlevel, $field, $PIP_switch), 'projectfilter.xlsx');
    }

}

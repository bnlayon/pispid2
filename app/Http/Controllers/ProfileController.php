<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {

        return view('profile');
    }

    // public function getChronoDates($id){
    //     $project_events = \App\Chronodatesprojects::join('projects', 'projects.id', '=', 'chronodatesprojects.proj_id')
    //                                 ->select('chronodatesprojects.date','chronodatesprojects.remarks')
    //                                 ->where('chronodatesprojects.proj_id', '=', $id)
    //                                 ->get();
    //     return $project_events;
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\ProjectExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Schema;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin()
    {
 
        $users = \App\User::all();
     // print_r($fields);
        return view('admin', compact('users'));
    }

    public function deactivate(Request $request, $id)
    {
        $user        = \App\User::find($id);
        $user->active = 0;
        $user->save();
     // print_r($fields);
        alert()->success('User Deactivated!')->persistent("Close");
        return back();
    }

    public function activate(Request $request, $id)
    {
        $user        = \App\User::find($id);
        $user->active = 1;
        $user->save();
     // print_r($fields);
        alert()->success('User Activated!')->persistent("Close");
        return back();
    }

    

}

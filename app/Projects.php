<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    public function getChronoDates($id){
        $project_events = \App\Chronodatesprojects::join('projects', 'projects.id', '=', 'chronodatesprojects.proj_id')
                                    ->select('chronodatesprojects.date','chronodatesprojects.remarks')
                                    ->where('chronodatesprojects.proj_id', '=', $id)
                                    ->get();
        return $project_events;
    }

    public function projects_subsectors() {
        return $this->belongsToMany('App\Subsectors', 'subsectorprojects', 'proj_id', 'subsector_id')->withTimestamps();
    }

    public function projects_estaffs() {
        return $this->belongsToMany('App\Evaluatingstaffs', 'evaluatingstaffsprojects', 'proj_id', 'staff_id')->withTimestamps();
    }

    public function projects_interregionals() {
        return $this->belongsToMany('App\Regions', 'regionprojects', 'proj_id', 'region_id')->withTimestamps();
    }

    public function projects_coiagencys() {
        return $this->belongsToMany('App\Agencies', 'coimpprojects', 'proj_id', 'coimp_id')->withTimestamps();
    }

    public function projects_icctb_dates() {
        return $this->belongsToMany('App\Icctbdatesprojects', 'icctbdatesprojects', 'proj_id', 'date')->withTimestamps();
    }

    public function projects_icctb_actions() {
        return $this->belongsToMany('App\Icctbdatesprojects', 'icctbdatesprojects', 'proj_id', 'remarks')->withTimestamps();
    }
    public function projects_othersectors() {
        return $this->belongsToMany('App\Othersectors', 'othersectors', 'proj_id', 'sector_id')->withTimestamps();
    }
    public function projects_documents() {
        return $this->belongsToMany('App\Projectdocuments', 'projectdocuments', 'proj_id', 'doc_id')->withTimestamps();
    }    
}
